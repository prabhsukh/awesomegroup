
<?php
$driver = 'mysql';
$database = "dbname=dbuser";
$dsn = "$driver:host=localhost;$database";

$username = 'root';
$password = '';



try {
   $conn = new PDO($dsn, $username, $password);
   echo "<h2>Database Connected<h2>";
}catch(PDOException $e){
   echo "<h1>" . $e->getMessage() . "</h1>";
}
$sql = 'SELECT  * FROM tbuser';
$stmt = $conn->prepare($sql);
$stmt->execute();

echo "<table style='width:50%'>";
while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
  echo "<tr>";
  foreach($row as $value)
  {
    echo sprintf("<td>%s</td>", $value);
  }
  echo "</tr>";
}
echo "</table>";
?>
