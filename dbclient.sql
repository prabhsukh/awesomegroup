-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: dbclient
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbclient`
--

DROP TABLE IF EXISTS `tbclient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclient` (
  `ClientID` int(11) NOT NULL AUTO_INCREMENT,
  `ClientName` varchar(45) DEFAULT NULL,
  `ClientEmail` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ClientID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclient`
--

LOCK TABLES `tbclient` WRITE;
/*!40000 ALTER TABLE `tbclient` DISABLE KEYS */;
INSERT INTO `tbclient` VALUES (1,'Kevin Grant','Grantman@yahoo.com'),(2,'Reginald Fairfield','morningstar@gmail.com'),(3,'Felicity Smoak','ihasemail@workplace.net'),(4,'Hugo Lloris','theworstgoalkeeper@totspurs.com'),(5,'Daenerys Targaryen','motherofdragons@gameofthrones.com'),(6,'Steven Universe','crystalgems@outlook.com'),(7,'Laura Palmer','blacklodge@twinpeaks.com');
/*!40000 ALTER TABLE `tbclient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbcommdetail`
--

DROP TABLE IF EXISTS `tbcommdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbcommdetail` (
  `CommdetailID` int(11) NOT NULL AUTO_INCREMENT,
  `MessageTo` varchar(45) DEFAULT NULL,
  `MessageFrom` varchar(45) DEFAULT NULL,
  `MessageSubject` varchar(45) DEFAULT NULL,
  `DateReceived` date DEFAULT NULL,
  `ClientID` int(11) DEFAULT NULL,
  PRIMARY KEY (`CommdetailID`),
  KEY `ClientID_idx` (`ClientID`),
  CONSTRAINT `ClientID` FOREIGN KEY (`ClientID`) REFERENCES `tbclient` (`ClientID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcommdetail`
--

LOCK TABLES `tbcommdetail` WRITE;
/*!40000 ALTER TABLE `tbcommdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbcommdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'dbclient'
--
/*!50003 DROP PROCEDURE IF EXISTS `spInsertClient` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `spInsertClient`(IN ClientName VARCHAR(50), IN ClientEmail VARCHAR(50))
BEGIN
SET @ClientName=ClientName;
SET @ClientEmail=ClientEmail;


INSERT INTO tbclient(ClientName,ClientEmail) VALUES (@ClientName, @ClientEmail);
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spInsertCommDetail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `spInsertCommDetail`(IN MessageTo VARCHAR(50), IN MessageFrom VARCHAR(50), in MessageSubject Varchar(50), In MessageBody longtext, In ClientID Int)
BEGIN
SET @MessageTo=MessageTo;
SET @MessageFrom=MessageFrom;
Set @MessageSubject = MessageSubject;
/*Set @MessageBody = MessageBody;*/
Set @ClientID = ClientID;

INSERT INTO tbcommdetail(MessageTo,MessageFrom,MessageSubject,MessageBody,ClientID) VALUES (@MessageTo,@MessageFrom,@MessageSubject,@MessageBody,@ClientID);
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-12 15:12:49
