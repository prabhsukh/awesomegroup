-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: dbemail
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbemail`
--

DROP TABLE IF EXISTS `tbemail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbemail` (
  `EmailID` int(11) NOT NULL AUTO_INCREMENT,
  `EmailMessage` longtext,
  `MessageTo` varchar(50) DEFAULT NULL,
  `MessageFrom` varchar(50) DEFAULT NULL,
  `DateReceived` date DEFAULT NULL,
  `MessageSubject` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`EmailID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbemail`
--

LOCK TABLES `tbemail` WRITE;
/*!40000 ALTER TABLE `tbemail` DISABLE KEYS */;
INSERT INTO `tbemail` VALUES (1,'What up','mark.remoto@robersoncollege.net','Grantman@yahoo.com','2015-03-24','!New update window'),(2,'Hello','motherofdragons@gameofthrones.com','prabhjot.prabhjot@ro','2015-04-12','Make a appointment'),(3,'Goodbye','jayren.bernasol@robertsoncollege.net','morningstar@gmail.com','2015-05-09','!Nice to see you again'),(4,'adsfa','theworstgoalkeeper@totspurs.com','kulwinder.brar@robertsoncollege.net','2015-02-18','HR Issue'),(5,'ASDF','ihasemail@workplace.net','veberly.carvalho@robertsoncollege.net','2015-03-21','Lunch?'),(6,'why you do this?','veberly.carvalho@robertsoncollege.net','theworstgoalkeeper@totspurs.com','2015-12-03',' requesting Subject)');
/*!40000 ALTER TABLE `tbemail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'dbemail'
--
/*!50003 DROP PROCEDURE IF EXISTS `spInsertEmail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `spInsertEmail`(IN EmailMessage Longtext, IN MessageTo VARCHAR(50), In MessageFrom Varchar(50), In DateReceived Date, In MessageSubject Varchar(50))
BEGIN
SET @EmailMessage=EmailMessage;
SET @MessageTo=MessageTo;
SET @MessageFrom=MessageFrom;
Set @DateReceived=DateReceived;
Set @MessageSubject = MessageSubject;

INSERT INTO tbemail(EmailMessage,MessageTo,MessageFrom,DateReceived,MessageSubject) VALUES
 (@EmailMessage, @MessageTo, @MessageFrom, @DateReceived,@MessageSubject);
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-08 15:49:41
